'use strict'

###*
 # @ngdoc overview
 # @name uiApp
 # @description
 # # uiApp
 #
 # Main module of the application.
###
app = angular
  .module('uiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'duScroll',
    'ui.bootstrap'
  ])
  .config ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
      .when '/history',
        templateUrl: 'views/history.html'
        controller: 'HistoryCtrl'
      .when '/pages',
        templateUrl: 'views/pages.html'
        controller: 'PagesCtrl'
      .otherwise
        redirectTo: '/'
