'use strict'

###*
 # @ngdoc function
 # @name uiApp.controller:PagesCtrl
 # @description
 # # PagesCtrl
 # Controller of the uiApp
###
angular.module('uiApp')
  .controller 'PagesCtrl', ($scope) ->
    $scope.awesomeThings = [
      'HTML5 Boilerplate'
      'AngularJS'
      'Karma'
    ]
