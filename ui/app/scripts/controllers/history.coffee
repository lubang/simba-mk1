'use strict'

###*
 # @ngdoc function
 # @name uiApp.controller:HistoryCtrl
 # @description
 # # HistoryCtrl
 # Controller of the uiApp
###
angular.module('uiApp')
  .controller 'HistoryCtrl', ($scope, $http) ->
    $scope.keywordList = ''

    promise = $http.post('/api/search/getHistory/')
      .success((data, status, headers, config) =>
        $scope.keywordList = data
      )
      .error((data, status, headers, config) =>
        $scope.keywordList = 'Fail to get keyword history'
      )
