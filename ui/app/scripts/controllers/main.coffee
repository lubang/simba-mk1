'use strict'

###*
 # @ngdoc function
 # @name uiApp.controller:MainCtrl
 # @description
 # # MainCtrl
 # Controller of the uiApp
###
angular.module('uiApp')
  .controller 'MainCtrl', ($scope, $http, $document, $modal) ->
    $scope.keyword = ''
    $scope.page = 0
    $scope.pageSize = 10
    $scope.pageCount = 0
    $scope.pageStart = 0
    $scope.pagePerScreen = 20
    $scope.data = ''
    
    $scope.defines = {}
    $scope.defines.words = []
    $scope.defines.contactHide = true
    $scope.defines.contact = {}
    $scope.defines.contactTemplate = 'views/preview/contacts.html'
    $scope.defines.contactVideoExist = false
    $scope.defines.contacts = {}
    $scope.defines.contacts.data = []

    $http.post('/api/contacts/list/')
      .success((data, status, headers, config) =>
        $scope.defines.contacts.data = data
        for item in data
          $scope.defines.words.push(item.name) if item.hasOwnProperty('name')
      )

    scrollTo = (elementId) ->
      $document.scrollToElement(
        angular.element(document.getElementById(elementId)), 
        30, 
        1000)

    showDefines = ->
      isShow = ($scope.keyword in $scope.defines.words)
      $scope.defines.contactHide = (isShow is false)

      if isShow
        $scope.defines.contact = $scope.defines.contacts.data[$scope.defines.words.indexOf($scope.keyword)]
        $scope.defines.contactPhoto = '/api/contacts/photo/' + $scope.defines.contact.email

        video = document.getElementById('contactVideo')
        video.src = '/api/contacts/video/' + $scope.defines.contact.email
        video.addEventListener('error', (event) -> $scope.defines.contactVideoExist = false)
        video.addEventListener('playing', (event) -> $scope.defines.contactVideoExist = true)
        video.load()            
        video.play()

    $scope.scrollToTop = ->      
      scrollTo('cover')

    $scope.keywordChanged = ->
      $scope.defines.contactVideoExist = false
      showDefines()

    $scope.search = (page) ->
      $scope.page = page
      params =
        'keyword': $scope.keyword
        'page': $scope.page
        'pageSize': $scope.pageSize
      promise = $http.post('/api/search/processSearch/', params)
        .success((data, status, headers, config) =>
          $scope.data = data
          $scope.pageCount = Math.ceil(data.hits.total / $scope.pageSize)
        )
        .error((data, status, headers, config) =>
          $scope.data = 'Fail to search'
          $scope.pageCount = 0
        )
      promise.then ->
        if $scope.pageCount < $scope.pagePerScreen
          $scope.pages = new Array($scope.pageCount)
          $scope.pageStart = 0
        else
          $scope.pages = new Array($scope.pagePerScreen)
          $scope.pageStart = $scope.page - $scope.pagePerScreen / 2
          if $scope.pageStart < 0
            $scope.pageStart = 0
        scrollTo('search-result')

    $scope.getDocUrl = (doc) ->
      return '/api/search/files/' + doc._type + '/' + doc._id

    $scope.download = (doc) ->
      window.open($scope.getDocUrl(doc))
      return true

    getContentType = (doc) ->
      if (!doc._source.hasOwnProperty('contentType'))
        path = doc._source.path.toLowerCase()
        if (path.match('(.mp4)$'))
          return 'video'
        else if (path.match('(.mp3|.ogg)$'))
          return 'audio'
        else if (path.match('(.jpg|.png|.bmp|.jpeg|.gif)$'))
          return 'image'
      else
        contentType = doc._source.contentType
        if (contentType.match('video/mp4'))
          return 'video'
        else if (contentType.match('audio'))
          return 'audio'
        else if (contentType.match('image'))
          return 'image'
      return 'document'

    $scope.preview = (doc) ->
      param = 
        path: $scope.getDocUrl(doc)
        content: doc._source.content
        contentType: getContentType(doc)

      $modal.open(
        templateUrl: 'views/preview/' + param.contentType + '.html'
        controller: 'PreviewCtrl'
        windowClass: 'preview'
        resolve:
          data: -> return param
      )

angular.module('uiApp')
  .controller 'PreviewCtrl', ($scope, $modalInstance, data) ->
    $scope.data = data
