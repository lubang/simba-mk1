'use strict'

describe 'Controller: KeywordsCtrl', ->

  # load the controller's module
  beforeEach module 'uiApp'

  KeywordsCtrl = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    KeywordsCtrl = $controller 'KeywordsCtrl', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(scope.awesomeThings.length).toBe 3
