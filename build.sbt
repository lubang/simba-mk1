import play.Project._
import com.tuplejump.sbt.yeoman.Yeoman

name := """simba-mk1"""

version := "1.0.0"

libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.2.0", 
  "org.webjars" % "bootstrap" % "2.3.1",
  "org.apache.tika" % "tika-parsers" % "1.6",
  "org.scalaj" %% "scalaj-http" % "0.3.16",
  "org.mongodb" %% "casbah" % "2.7.3",
  "com.github.nscala-time" %% "nscala-time" % "1.4.0"
)

playScalaSettings

Yeoman.yeomanSettings

