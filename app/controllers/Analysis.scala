package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._

import models._

object Analysis extends Controller {

  def perform = Action {
    AnalysisEngine.perform
    Ok("Complete to analysis.")
  } 

}