package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._

import scalaj.http.{Http, HttpOptions, HttpException}

import models._
import persistances._

object Application extends Controller {

  def processSearch = Action(parse.json) { request =>
    val keyword = (request.body \ "keyword").asOpt[String].get
    val page = (request.body \ "page").asOpt[Int].get
    val pageSize = (request.body \ "pageSize").asOpt[Int].get
    
    val json = Json.obj(
      "query" -> Json.obj(
        "query_string" -> Json.obj(
          "fields" -> Json.arr("url^3", "content"),
          "query" -> keyword
        )
      ),
      "from" -> page * pageSize,
      "size" -> pageSize      
    )

    if (page == 0)
      MongoHelper.createHistory(keyword)

    val response = Http.postData(AppConfig.getUrlForIndex + "_search", json.toString)
      .option(HttpOptions.connTimeout(2000))
      .option(HttpOptions.readTimeout(10000))
      .header("content-type", "application/json")
      .asString
    Ok(response)
  }

  def download(esType: String, esId: String) = Action {
    val response = Http(AppConfig.getUrlForIndex + esType + "/" + esId).asString
    val json: JsValue = Json.parse(response)
    Ok.sendFile(new java.io.File((json \ "_source" \ "path").as[String]))
  }

  def getHistory = Action {
  	Ok(MongoHelper.readHistory(30))
  }
  
}