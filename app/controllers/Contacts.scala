package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._

import scalaj.http.{Http, HttpOptions, HttpException}

import models._
import persistances._

object Contacts extends Controller {
  
  def getList = Action {
    Ok(MongoHelper.readContacts())
  }

  def getPhoto(id: String) = Action {
    val file = new java.io.File(AppConfig.resourceDir, "contacts/profile/" + id + ".jpg")
    if (file.exists())
      Ok.sendFile(file)
    else
      NotFound("File is not exists")
  }  

  def getVideo(id: String) = Action {
  	val file = new java.io.File(AppConfig.resourceDir, "contacts/video/" + id + ".mp4")
  	if (file.exists())
  	  Ok.sendFile(file)
  	else
  	  NotFound("File is not exists")
  }  
}