package persistances

import java.io.File
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.conversions.scala._
import com.github.nscala_time.time.Imports._

object MongoHelper {
  val mongoClient = MongoClient("localhost", 27017)
  val db = mongoClient("simba-mk1")
  val collKeywordName = "keyword"
  val collContactsName = "contacts"
  
  def createHistory(keyword: String) {
    val coll = db(collKeywordName)
  	val obj = MongoDBObject("time" -> DateTime.now.toString) ++ ("keyword" -> keyword)
    coll.insert(obj)
  }

  def readHistory(limitSize: Integer) = {
    val coll = db(collKeywordName)
    com.mongodb.util.JSON.serialize(coll.find().sort(MongoDBObject("time" -> -1)).limit(limitSize).toList)
  }

  def readContacts() = {
    val coll = db(collContactsName)
    com.mongodb.util.JSON.serialize(coll.find().toList) 
  }
}