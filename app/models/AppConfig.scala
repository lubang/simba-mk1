package models

object AppConfig {
  var resourceDir: String = null				// resource dir
  var esUrl: String	= null					  	// elsastic search url
  var esIndex: String = null

  var isOnAnalysis: Boolean = false
  var crawlerParams: List[AnalysisEngineConfig] = Nil 	// Crawling Dir list

  def getUrlForIndex: String = {
    esUrl + "/" + esIndex + "/"
  }
}