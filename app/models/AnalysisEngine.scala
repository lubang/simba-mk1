package models

import java.io._

import play.api.Logger
import play.api.libs.json._

import scalaj.http.{Http, HttpOptions, HttpException}
import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.ParseContext
import org.apache.tika.parser.Parser
import org.apache.tika.parser.AutoDetectParser
import org.apache.tika.sax.BodyContentHandler

import persistances._

object AnalysisEngine {
  private val md = java.security.MessageDigest.getInstance("MD5");

  def perform() {
    Logger.info("Start")
    try {
      for (param <- AppConfig.crawlerParams) {
        crawlDirectory(new File(param.dir), param)
      }
      Logger.info("Completed")
    } catch {
      case e:Exception => Logger.error("Fail to process crawling")
    }
  }

  private def crawlDirectory(file: File, param: AnalysisEngineConfig) {
    Logger.info("Dir: " + file.getPath)

    try {
      file.listFiles.map { file: File =>
        if (file.isDirectory) crawlDirectory(file, param)
        else crawlFile(file, param)
      }
    } catch {
      case e:Exception => Logger.error("Fail to crawlDirectory")
    }
  }

  private def crawlFile(file: File, param: AnalysisEngineConfig) {
    if (checkExistIndex(file, param)) {
      Logger.debug("Path (PASS): " + file.getPath)
      return
    } else {
      Logger.debug("Path: " + file.getPath)
    }

    var reqJson = JsObject(
      Seq(
        "path" -> JsString(file.getPath),
        "url" -> JsString(getPathForWeb(file, param)),
        "updateDate" -> JsNumber(file.lastModified())
      )
    )

    if (validFile(file)) {
      val (isExtract: Boolean, contentType: String, content: String, meta: String) = extractContent(file)
      if (isExtract) {
        reqJson = reqJson + ("contentType" -> JsString(contentType))
        reqJson = reqJson + ("content" -> JsString(content))
        reqJson = reqJson + ("meta" -> JsString(meta))
      }
    }

    try {
      Http.postData(getElasticSearchUrl(file, param), reqJson.toString)
        .header("content-type", "application/json")
        .responseCode
    } catch {
      case e: Exception => Logger.error("Fail to send data(" + file.getPath + ") to elasticsearch.", e)
    }
  }

  private def getPathForWeb(file: File, param: AnalysisEngineConfig): String = {
    file.getPath.replace(param.dir, param.dirWebPath)
  }

  private def getElasticSearchUrl(file: File, param: AnalysisEngineConfig): String = {
    val hexType = md.digest(param.dir.getBytes("UTF-8"))
    val esType = new java.math.BigInteger(1, hexType).toString(16)
    val hexId = md.digest(file.getPath.getBytes("UTF-8"))
    val esId = new java.math.BigInteger(1, hexId).toString(16)

    AppConfig.getUrlForIndex + esType + "/" + esId
  }

  private def checkExistIndex(file: File, param: AnalysisEngineConfig): Boolean = {
    try {
      val resExistIndex: JsValue  = Json.parse(Http(getElasticSearchUrl(file, param)).asString)

      val isFound: Boolean = (resExistIndex \ "found").as[Boolean]
      isFound && ((resExistIndex \ "_source" \ "updateDate").as[Long] == file.lastModified())
    } catch {
      case e: Exception => false
    }
  }

  private def validFile(file: File): Boolean = {
    val excludeExts = List() // List(".raw", ".lnk", "MP3/013.MP3")
    val excludeWords = List(".Trashes", ".DS_Store", "Thumbs.db", ".TemporaryItems", ".apdisk", "$RECYCLE.BIN")

    (file.length() < 1024 * 1024 * 20) && !(excludeWords.exists(file.getPath.contains)) && !excludeExts.exists(file.getPath.endsWith)
  }

  private def extractContent(file: File): (Boolean, String, String, String) = {
    try {
      val parser = new AutoDetectParser
      val metadata = new Metadata
      val parseContext = new ParseContext
      val handler = new BodyContentHandler
      parser.parse(new FileInputStream(file), handler, metadata, parseContext)

      val contentType: String = metadata.get("Content-Type")
      val content: String = handler.toString
      val meta: String = metadata.names.map { name => name + ":" + metadata.get(name)}.mkString(", ")

      (true, contentType, content, meta)
    } catch {
      case e: Exception => {
        Logger.error("Fail to parse document (" + file.getPath + ")")
        (false, null, null, null)
      }
      case e: OutOfMemoryError => {
        Logger.error("Fail to parse document (" + file.getPath + ") because of memory")
        (false, null, null, null)
      }
    }
  }
}
