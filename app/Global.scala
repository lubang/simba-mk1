import play.api.Application
import play.api.GlobalSettings
import play.api.Logger
import play.api.Play


import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.concurrent.Akka
import scala.concurrent.duration.DurationInt
import akka.actor.Actor
import akka.actor.Props

import models._

object Global extends GlobalSettings {
  
  override def onStart(app: Application) {
    Logger.info("Scheduling started")

    AppConfig.resourceDir =  app.configuration.getString("config.resourceDir").get
    AppConfig.esUrl = app.configuration.getString("config.esUrl").get
    AppConfig.esIndex = app.configuration.getString("config.esIndex").get
    AppConfig.isOnAnalysis = app.configuration.getBoolean("config.isOnAnalysis").get

    var params = app.configuration.getObjectList("config.crawlerParams").get
    for (i <- 0 until params.size()) {
      var param = new AnalysisEngineConfig(
        params.get(i).get("dir").unwrapped.toString,
        params.get(i).get("dirForWeb").unwrapped.toString)
      AppConfig.crawlerParams = param :: AppConfig.crawlerParams;
    }

    play.api.Play.mode(app) match {
      case play.api.Mode.Test => // do not schedule anything for Test
      case _ => startSchedule(app)
    }
  }

  private def startSchedule(app: Application) = {    
    
    val tick = "tick"
    val actor = Akka.system(app).actorOf(Props(new Actor {
      def receive = {
        case tick => { if (AppConfig.isOnAnalysis) AnalysisEngine.perform }
      }
    }))

    Akka.system(app).scheduler.schedule(0 seconds, 
      1200 minutes, 
      actor, 
      tick) 
  }
}